#ifndef MATRIX
#define MATRIX

#include <iostream>
#include <vector>

using namespace std;

void error(string msg);

class Matrix{
	private:
	int width;
	int height;
	vector<vector<float>> grid;

	public:
	Matrix(int width, int height);

	Matrix operator+(const Matrix& matrix);
	Matrix operator*(const Matrix& matrix);
	Matrix operator*(const int x);
	void operator<<(const vector<vector<float>> grid);

	float get(int i, int j);
	void set(int i, int j, float value);

	int getWidth();
	int getHeight();

	static void print(Matrix matrix);
};

Matrix operator*(const int x, Matrix& matrix);

#endif //MATRIX
