#include "matrix.h"

void error(string msg){
	cout<<"Matrix.h exception: "<<msg<<endl;
	exit(1);
}

Matrix::Matrix(int width, int height){
	if(width <= 0 || height <= 0){
		error("Invalid size.");
	}
	this->width = width;
	this->height = height;
	for(int i=0; i<height; i++){
		vector<float> foo;
		for(int j=0; j<width; j++){
			foo.push_back(int(i==j));
		}
		this->grid.push_back(foo);
	}
}

Matrix Matrix::operator+(const Matrix& matrix){
	if(this->width != matrix.width || this->height != matrix.height){
		error("Invalid size.");
	}
	Matrix foo(this->width, this->height);
	for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			foo.set(i, j, this->grid[i][j] + matrix.grid[i][j]);
		}
	}
	return foo;
}

Matrix Matrix::operator*(const Matrix& matrix){
	if(width != matrix.height){
		error("Invalid size.");
	}
	Matrix foo(matrix.width, height);
	for(int i=0; i<height; i++){
		for(int j=0; j<matrix.width; j++){
			int sum = 0;
			for(int s=0; s<width; s++){
				sum += grid[i][s] * matrix.grid[s][j];
			}
			foo.set(i, j, sum);
		}
	}
	return foo;
}

Matrix Matrix::operator*(const int x){
	Matrix foo(width, height);
	for(int i=0; i<height; i++){
		for(int j=0; j<width; j++){
			foo.set(i, j, grid[i][j] * x);
		}
	}
	return foo;
}

Matrix operator*(const int x, Matrix& matrix){
	return matrix*x;
}

void Matrix::operator<<(const vector<vector<float>> grid){
	if(this->width != grid[0].size() || this->height != grid.size()){
		error("Invalid size.");
	}
	for(int i=0; i<grid.size(); i++){
		for(int j=0; j<grid[0].size(); j++){
			this->grid[i][j] = grid[i][j];
		}
	}
}

float Matrix::get(int i, int j){
	return this->grid[i][j];
}

void Matrix::set(int i, int j, float value){
	if(i > height || j > width){
		error("Out of range.");
	}
	this->grid[i][j] = value;
}

int Matrix::getWidth(){
	return width;
}

int Matrix::getHeight(){
	return height;
}

void Matrix::print(Matrix matrix){
	for(int i=0; i<matrix.getHeight(); i++){
		for(int j=0; j<matrix.getWidth(); j++){
			cout<<matrix.get(i, j)<<" ";
		}
		cout<<"\n";
	}
}
